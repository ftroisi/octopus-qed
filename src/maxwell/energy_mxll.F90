!! Copyright (C) 2020 F. Bonafe, R. Jestaedt, H. Appel, N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module energy_mxll_oct_m
  use debug_oct_m
  use mesh_oct_m
  use global_oct_m
  use messages_oct_m
  use profiling_oct_m
  use states_mxll_oct_m

  implicit none

  private

  public ::               &
    energy_mxll_t,        &
    energy_density_calc


  type energy_mxll_t
    ! Components are public by default
    ! Energies
    FLOAT                          :: energy = M_ZERO
    FLOAT                          :: boundaries = M_ZERO
    FLOAT                          :: e_energy = M_ZERO
    FLOAT                          :: b_energy = M_ZERO
    FLOAT                          :: energy_plane_waves = M_ZERO
    FLOAT                          :: e_energy_plane_waves = M_ZERO
    FLOAT                          :: b_energy_plane_waves = M_ZERO

    FLOAT                          :: energy_trans = M_ZERO
    FLOAT                          :: energy_long = M_ZERO
    FLOAT                          :: e_energy_trans = M_ZERO
    FLOAT                          :: b_energy_trans = M_ZERO
    FLOAT                          :: energy_incident_waves = M_ZERO
  end type energy_mxll_t

contains

  subroutine energy_density_calc(mesh, st, rs_field, energy_dens, e_energy_dens, b_energy_dens, plane_waves_check, &
    rs_field_plane_waves, energy_dens_plane_waves)
    class(mesh_t),       intent(in)    :: mesh
    type(states_mxll_t), intent(in)    :: st
    CMPLX,               intent(in)    :: rs_field(:,:)
    FLOAT,               intent(inout) :: energy_dens(:)
    FLOAT,               intent(inout) :: e_energy_dens(:)
    FLOAT,               intent(inout) :: b_energy_dens(:)
    logical,   optional, intent(in)    :: plane_waves_check
    CMPLX,     optional, intent(in)    :: rs_field_plane_waves(:,:)
    FLOAT,     optional, intent(inout) :: energy_dens_plane_waves(:)

    integer            :: idim, ip
    type(profile_t), save :: prof

    PUSH_SUB(energy_density_calc)

    call profiling_in(prof, 'ENERGY_DENSITY_CALC')

    e_energy_dens(:) = M_ZERO
    b_energy_dens(:) = M_ZERO
    do ip = 1, mesh%np
      do idim = 1, st%dim
        e_energy_dens(ip) = e_energy_dens(ip) + real(rs_field(ip,idim))**2
        b_energy_dens(ip) = b_energy_dens(ip) + aimag(rs_field(ip,idim))**2
      end do
      energy_dens(ip) = e_energy_dens(ip) + b_energy_dens(ip)
    end do

    if (present(rs_field_plane_waves) .and. present(energy_dens_plane_waves) .and. &
      optional_default(plane_waves_check, .false.)) then
      energy_dens_plane_waves(:) = M_ZERO
      do ip = 1, mesh%np
        do idim = 1, st%dim
          energy_dens_plane_waves(ip) = energy_dens_plane_waves(ip) &
            + real(conjg(rs_field_plane_waves(ip,idim)) * rs_field_plane_waves(ip,idim))
        end do
      end do
    end if

    call profiling_out(prof)

    POP_SUB(energy_density_calc)
  end subroutine energy_density_calc
end module energy_mxll_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
