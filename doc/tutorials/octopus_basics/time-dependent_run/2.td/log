    <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
                                ___
                             .-'   `'.
                            /         \
                            |         ;
                            |         |           ___.--,
                   _.._     |0) ~ (0) |    _.---'`__.-( (_.
            __.--'`_.. '.__.\    '--. \_.-' ,.--'`     `""`
           ( ,.--'`   ',__ /./;   ;, '.__.'`    __
           _`) )  .---.__.' / |   |\   \__..--""  """--.,_
          `---' .'.''-._.-'`_./  /\ '.  \ _.-~~~````~~~-._`-.__.'
                | |  .' _.-' |  |  \  \  '.               `~---`
                 \ \/ .'     \  \   '. '-._)
                  \/ /        \  \    `=.__`~-.
             jgs  / /\         `) )    / / `"".`\
            , _.-'.'\ \        / /    ( (     / /
             `--~`   ) )    .-'.'      '.'.  | (
                    (/`    ( (`          ) )  '-;
                     `      '-;         (-'

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA

    <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

                           Running octopus

Version                : bimaculatus
Commit                 : 865262acca2f58a8c1865f3eacd24f259041f926
Configuration time     : Tue Jun  6 13:47:35 CEST 2023
Configuration options  : maxdim3 sse2 avx libxc5 libxc_fxc
Optional libraries     : cgal nfft pspio
Architecture           : x86_64
C compiler             : gcc
C compiler flags       : -Wall -O2 -march=native -fprofile-arcs -ffpe-trap=zero,invalid
C++ compiler           : g++
C++ compiler flags     : -Wall -O2 -march=native -fprofile-arcs -ffpe-trap=zero,invalid
Fortran compiler       : gfortran (GCC version 10.3.0)
Fortran compiler flags : -g -Wall -fno-var-tracking-assignments -Wno-maybe-uninitialized -Wno-unused-dummy-argument -Wno-c-binding-type -Wno-surprising -Wno-maybe-uninitialized -O2 -march=native -fbacktrace -fprofile-arcs -ffpe-trap=zero,invalid -finit-int

             The octopus is swimming in poppyseed (Linux)


            Calculation started on 2023/06/06 at 17:07:17


************************** Calculation Mode **************************
Input: [CalculationMode = td]
**********************************************************************


******************************* Space ********************************
Octopus will run in 3 dimension(s).
Octopus will treat the system as periodic in 0 dimension(s).
**********************************************************************

Input: [AllElectronType = no]
Input: [PseudopotentialSet = standard]
Reading Coordinates from Coordinates block

****************************** Species *******************************
  Species 'C'
    type             : pseudopotential
    file             : '/home/luedersm/Octopus_foss-2021a/share/octopus/pseudopotentials/PSF/C.psf'
    file format      : PSF
    valence charge   : 4.0
    atomic number    :   6
    form on file     : semilocal
    orbital origin   : calculated
    lmax             : 1
    llocal           : 0
    projectors per l : 1
    total projectors : 1
    application form : kleinman-bylander
    orbitals         : 16
    bound orbitals   :  4

  Species 'H'
    type             : pseudopotential
    file             : '/home/luedersm/Octopus_foss-2021a/share/octopus/pseudopotentials/PSF/H.psf'
    file format      : PSF
    valence charge   : 1.0
    atomic number    :   1
    form on file     : semilocal
    orbital origin   : calculated
    lmax             : 0
    llocal           : 0
    projectors per l : 1
    total projectors : 0
    application form : local
    orbitals         : 16
    bound orbitals   :  1

**********************************************************************


***************************** Symmetries *****************************
Symmetry elements : 4*(C3) 3*(C2) 3*(S4) 6*(sigma)
Symmetry group    : Td
**********************************************************************

Input: [CurvMethod = curv_affine]
Input: [DerivativesStencil = stencil_star]
Input: [SpinComponents = unpolarized]
Input: [SmearingFunction = semiconducting]
Input: [SymmetrizeDensity = no]

******************************* States *******************************
Total electronic charge  =        8.000
Number of states         =        4
States block-size        =        4
**********************************************************************


************************** Parallelization ***************************
Info: Octopus will run in *serial*
**********************************************************************

Info: Generating weights for finite-difference discretization of x-gradient
Info: Generating weights for finite-difference discretization of y-gradient
Info: Generating weights for finite-difference discretization of z-gradient
Info: Generating weights for finite-difference discretization of Laplacian

******************************** Grid ********************************
Simulation Box:
  Type = minimum
  Radius  [A] =   3.500
Main mesh:
  Spacing [A] = ( 0.180, 0.180, 0.180)    volume/point [A^3] =      0.00583
  # inner mesh =      55241
  # total mesh =      83697
  Grid Cutoff [eV] =  1160.586810    Grid Cutoff [Ry] =    85.301565
**********************************************************************

Info: states-block size = 2.6 MiB
Input: [StatesPack = yes]
Input: [StatesOrthogonalization = cholesky_serial]
Info: the XCFunctional has been selected to match the pseudopotentials
      used in the calculation.

**************************** Theory Level ****************************
Input: [TheoryLevel = kohn_sham]

Exchange-correlation:
  Exchange
    Slater exchange (LDA)
    [1] P. A. M. Dirac, Math. Proc. Cambridge Philos. Soc. 26, 376 (1930)
    [2] F. Bloch, Z. Phys. 57, 545 (1929)
  Correlation
    Perdew & Zunger (Modified) (LDA)
    [1] J. P. Perdew and A. Zunger, Phys. Rev. B 23, 5048 (1981), modified to improve the matching between the low- and high-rs parts

**********************************************************************


****************************** Hartree *******************************
Input: [DressedOrbitals = no]
The chosen Poisson solver is 'interpolating scaling functions'
**********************************************************************

Input: [FilterPotentials = filter_TS]
Info: Pseudopotential for C
  Radii for localized parts:
    local part     =  1.1 A
    non-local part =  1.0 A
    orbitals       =  4.8 A

Info: Pseudopotential for H
  Radii for localized parts:
    local part     =  1.2 A
    non-local part =  0.0 A
    orbitals       =  4.6 A

Input: [RelativisticCorrection = non_relativistic]
Input: [DFTULevel = dft_u_none]
Input: [MagneticConstrain = 0]

****************** Approximate memory requirements *******************
Mesh
  global  :       1.3 MiB
  local   :       3.2 MiB
  total   :       4.5 MiB

States
  real    :       2.6 MiB (par_kpoints + par_states + par_domains)
  complex :       5.1 MiB (par_kpoints + par_states + par_domains)

**********************************************************************

Info: Generating external potential
      done.
Info: Octopus initialization completed.
Info: Starting calculation mode.
Input: [TDIonicTimeScale = 1.000]
Input: [IonsConstantVelocity = no]
Input: [Thermostat = none]
Input: [MoveIons = no]
Input: [TDTimeStep = 0.2000E-02 hbar/eV]
Input: [TDPropagationTime = 0.1000 hbar/eV]
Input: [TDMaxSteps = 50]
Input: [TDDynamics = ehrenfest]
Input: [TDScissor = 0.000]
Input: [TDPropagator = aetrs]
Input: [TDExponentialMethod = taylor]
Info: Blocks of states
      Block       1 contains       4 states:       1 -       4
Info: Could not find 'restart/td' directory for restart.
Info: No restart information will be read.
Info: Ground-state restart information will be read from 'restart/gs'.

           Info: Reading states: gs. 2023/06/06 at 17:07:18

ETA: .......1......2.......3......4......5.......6......7.......8......9......0

          Info: States reading done. 2023/06/06 at 17:07:18

Info: Finished reading information from 'restart/gs'.
Info: Time-dependent restart information will be written to 'restart/td'.

********************* Time-Dependent Simulation **********************
  Iter           Time        Energy   SC Steps    Elapsed Time
**********************************************************************

      1       0.002000   -218.870643         1         0.054
      2       0.004000   -218.870643         1         0.054
      3       0.006000   -218.870643         1         0.057
      4       0.008000   -218.870643         1         0.055
      5       0.010000   -218.870643         1         0.054
      6       0.012000   -218.870643         1         0.054
      7       0.014000   -218.870643         1         0.057
      8       0.016000   -218.870643         1         0.055
      9       0.018000   -218.870643         1         0.053
     10       0.020000   -218.870643         1         0.058
     11       0.022000   -218.870643         1         0.053
     12       0.024000   -218.870643         1         0.059
     13       0.026000   -218.870643         1         0.054
     14       0.028000   -218.870643         1         0.053
     15       0.030000   -218.870643         1         0.055
     16       0.032000   -218.870643         1         0.053
     17       0.034000   -218.870643         1         0.055
     18       0.036000   -218.870643         1         0.053
     19       0.038000   -218.870643         1         0.054
     20       0.040000   -218.870643         1         0.060
     21       0.042000   -218.870643         1         0.054
     22       0.044000   -218.870643         1         0.053
     23       0.046000   -218.870643         1         0.058
     24       0.048000   -218.870643         1         0.054
     25       0.050000   -218.870643         1         0.063
     26       0.052000   -218.870643         1         0.055
     27       0.054000   -218.870643         1         0.058
     28       0.056000   -218.870643         1         0.054
     29       0.058000   -218.870643         1         0.056
     30       0.060000   -218.870643         1         0.064
     31       0.062000   -218.870643         1         0.055
     32       0.064000   -218.870643         1         0.053
     33       0.066000   -218.870643         1         0.054
     34       0.068000   -218.870643         1         0.054
     35       0.070000   -218.870643         1         0.054
     36       0.072000   -218.870643         1         0.057
     37       0.074000   -218.870643         1         0.054
     38       0.076000   -218.870643         1         0.053
     39       0.078000   -218.870643         1         0.057
     40       0.080000   -218.870643         1         0.061
     41       0.082000   -218.870643         1         0.056
     42       0.084000   -218.870643         1         0.064
     43       0.086000   -218.870643         1         0.112
     44       0.088000   -218.870643         1         0.064
     45       0.090000   -218.870643         1         0.077
     46       0.092000   -218.870643         1         0.058
     47       0.094000   -218.870643         1         0.053
     48       0.096000   -218.870643         1         0.053
     49       0.098000   -218.870643         1         0.054
     50       0.100000   -218.870643         1         0.058

             Info: Writing states. 2023/06/06 at 17:07:21


        Info: Finished writing states. 2023/06/06 at 17:07:21

Info: Finished writing information to 'restart/td'.

             Calculation ended on 2023/06/06 at 17:07:21

                          Walltime:  03.867s

Octopus emitted 1 warning.
