set pm3d
set view map
set palette defined (-0.005 "blue", 0 "white", 0.005 "red")
set term png size 1000,500

unset surface
unset key

set output 'tutorial_03.1-plot_current_2d.png'

set cbrange [-0.05:0.05]

set multiplot

set origin 0.025,0
set size 0.3,0.9
set size square
set title 'External current density J_z - step 30'
sp [-10:10][-10:10] 'Maxwell/output_iter/td.0000030/external_current-z.z=0' u 1:2:3

set origin 0.35,0
set size 0.3,0.9
set size square
set title 'External current density J_z - step 90'
sp [-10:10][-10:10] 'Maxwell/output_iter/td.0000090/external_current-z.z=0' u 1:2:3

set origin 0.675,0
set size 0.3,0.9
set size square
set title 'External current density J_z - step 150'
sp [-10:10][-10:10] 'Maxwell/output_iter/td.0000150/external_current-z.z=0' u 1:2:3

unset multiplot


