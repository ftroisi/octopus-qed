#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt

cp $OCTOPUS_TOP/testsuite/maxwell/01-free-propagation.01-1_pulse_td.inp inp
sed -i 's:TDPropagationTime .*:TDPropagationTime                 = 0.35:' inp

octopus > log

gnuplot plot1.gnu
gnuplot plot2.gnu

cp inp *.txt *.gnu tutorial.sh $OCTOPUS_TOP/doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/

cp *.png $OCTOPUS_TOP/doc/html/images/Maxwell/
