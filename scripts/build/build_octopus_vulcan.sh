#!/bin/bash
export LIBS_BLAS="-L/usr/local/tools/essl/5.1/lib/ -lesslsmpbg"
export LIBS_LAPACK="/usr/local/tools/lapack/lib/liblapack.a"
export LIBS_FFT="-L/usr/local/tools/fftw-3.3.3/lib/ -lfftw3_omp"
export FC_INTEGER_SIZE=4
export CC_FORTRAN_INT=int
export CC=mpixlc_r
export FC=mpixlf95_r
export LDFLAGS="-qsmp=omp"
export CFLAGS="-g -O3"
export FCFLAGS=$CFLAGS" -qxlf90=autodealloc -qessl"
./configure --with-libxc-prefix=$HOME --prefix=$HOME --host=powerpc32-unknown-linux-gnu --build=powerpc64-unknown-linux-gnu --with-gsl-prefix=$HOME --enable-openmp --enable-mpi
